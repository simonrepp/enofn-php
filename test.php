<?php declare(strict_types=1);

require('fn.php');

use function Enofn\parse;

$input = <<<DOC
> Comment

Field: Value

Field:
- Item
- Item

Field:
Attribute = Value
Attribute = Value

-- Embed
Multiline-
Value
-- Embed

Flag

# Section
## Subsection
### etc.

`Escaped`: Value

`` `Doubly-Escaped` ``: Value

Field:
`Escaped` = Value

> Add additional dashes to escape embed content
--- Embed
-- Embed
--- Embed

`Escaped`
DOC;

$result = parse($input);

print_r($result);

