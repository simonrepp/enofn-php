# enofn

A minimal eno parser.

enofn provides an eno parser implementation as a single function, in a single file.
The parse function returns the parsed contents as a plain object (stdClass), or throws in the event of a parse error, providing a minmal but useful error hint.

To get started it is recommended to parse your document and inspect the result,
then write your code around that:

```php
require('fn.php');

use function Enofn\parse;

$result = parse('greeting: hello');

print_r($result);
```