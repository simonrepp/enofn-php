<?php declare(strict_types=1);

namespace Enofn;

use Exception, stdClass;

class Parser {
    private const OPTIONAL = '([^\\n]+?)?';
    private const REQUIRED = '(\\S[^\\n]*?)';
    
    //
    // >
    // > comment
    private const COMMENT_OR_EMPTY_LINE = '(|>[^\\n]*)';
    public const COMMENT_OR_EMPTY_LINE_INDEX = 1;
    
    // - value
    private const ITEM = '(-)(?!-)[^\\S\\n]*'.self::OPTIONAL;
    public const ITEM_OPERATOR_INDEX = 2;
    public const ITEM_VALUE_INDEX = 3;
    
    // -- key
    private const EMBED = '(--++)[^\\S\\n]*'.self::REQUIRED;
    public const EMBED_OPERATOR_INDEX = 4;
    public const EMBED_KEY_INDEX = 5;
    
    // # key
    private const SECTION = '(#++)[^\\S\\n]*'.self::REQUIRED;
    public const SECTION_OPERATOR_INDEX = 6;
    public const SECTION_KEY_INDEX = 7;
    
    private const EARLY_DETERMINED = self::ITEM.'|'.self::EMBED.'|'.self::SECTION;
    
    // key
    private const KEY_UNESCAPED = '([^\\s>#\\-`:=][^\\n:=]*?)';
    public const KEY_UNESCAPED_INDEX = 8;
    
    // `key`
    public const KEY_ESCAPE_BEGIN_OPERATOR_INDEX = 9;
    private const KEY_ESCAPED = '(`++)[^\\S\\n]*(\\S[^\\n]*?)[^\\S\\n]*(\\'.self::KEY_ESCAPE_BEGIN_OPERATOR_INDEX.')';
    public const KEY_ESCAPED_INDEX = 10;
    public const KEY_ESCAPE_END_OPERATOR_INDEX = 11;
    
    private const KEY = '(?:'.self::KEY_UNESCAPED.'|'.self::KEY_ESCAPED.')';
    
    // :
    // : value
    private const FIELD = '(:)[^\\S\\n]*'.self::OPTIONAL;
    public const FIELD_OPERATOR_INDEX = 12;
    public const FIELD_VALUE_INDEX = 13;
    
    // =
    // = value
    private const ATTRIBUTE = '(=)[^\\S\\n]*'.self::OPTIONAL;
    public const ATTRIBUTE_OPERATOR_INDEX = 14;
    public const ATTRIBUTE_VALUE_INDEX = 15;
    
    private const LATE_DETERMINED = self::KEY.'\\s*(?:'.self::FIELD.'|'.self::ATTRIBUTE.')?';
    
    private const TO_DETERMINE = '(?:'.self::EARLY_DETERMINED.'|'.self::LATE_DETERMINED.')';
    
    public const LINE_REGEX = '/[^\\S\\n]*(?:'.self::COMMENT_OR_EMPTY_LINE.'|'.self::TO_DETERMINE.')[^\\S\\n]*(?=\\n|$)/';

    private int $index = 0;
    private ?stdClass $instruction;
    private ?stdClass $last_field = null;
    private stdClass $last_section;
    private int $line = 1;
    private ?array $match = null;
    
    function __construct(string $input) {
        $this->input = $input;
        $this->document_instruction = (object) [ 'type' => 'document' ];
        $this->result = [];
        $this->instructions = [];
        $this->last_section = $this->document_instruction;
        $this->active_section_instructions = [$this->document_instruction];
    }
    
    function parse() : stdClass {
        $embed = false;
        
        while ($this->index < strlen($this->input)) {
            $matched = preg_match(
                self::LINE_REGEX,
                $this->input,
                $this->match,
                PREG_OFFSET_CAPTURE | PREG_UNMATCHED_AS_NULL,
                $this->index
            );
            
            if ($matched != 1 || $this->match[0][1] != $this->index) {
                throw new Exception("Line {$this->line} is invalid");
            }
            
            if (isset($this->match[self::EMBED_OPERATOR_INDEX][0])) {
                $this->readEmbed();
            } else {
                if (isset($this->match[self::COMMENT_OR_EMPTY_LINE_INDEX][0])) {
                    // noop
                } else if (isset($this->match[self::FIELD_OPERATOR_INDEX][0])) {
                    $this->readField();
                } else if (isset($this->match[self::ITEM_OPERATOR_INDEX][0])) {
                    $this->readItem();
                } else if (isset($this->match[self::ATTRIBUTE_OPERATOR_INDEX][0])) {
                    $this->readAttribute();
                } else if (isset($this->match[self::SECTION_OPERATOR_INDEX][0])) {
                    $this->readSection();
                } else if (isset($this->match[self::EMBED_OPERATOR_INDEX][0])) {
                    $embed = true;
                    $this->readEmbed();
                } else {
                    $this->readFlag();
                }

                $instruction_length = $this->match[0][1] + strlen($this->match[0][0]) - $this->index;
                $this->index += $instruction_length + 1;
            }
            
            $this->line++;
        }
        
        return $this->document_instruction;
    }

    private function readAttribute() : void {
        if ($this->last_field === null) {
            throw new Exception("Attribute outside field in line {$this->line}");
        }
        
        $attribute = (object) [ 'type' => 'attribute' ];
        
        if (isset($this->match[self::KEY_UNESCAPED_INDEX][0])) {
            $attribute->key = $this->match[self::KEY_UNESCAPED_INDEX][0];
        } else {
            $attribute->key = $this->match[self::KEY_ESCAPED_INDEX][0];
        }
        
        if (isset($this->match[self::ATTRIBUTE_VALUE_INDEX][0])) {
            $attribute->value = $this->match[self::ATTRIBUTE_VALUE_INDEX][0];
        }
        
        if (property_exists($this->last_field, 'attributes')) {
            $this->last_field->attributes[] = $attribute;
        } else if (property_exists($this->last_field, 'items') || property_exists($this->last_field, 'value')) {
            throw new Exception("Mixed field content in line {$this->line}");
        } else {
            $this->last_field->attributes = [$attribute];
        }
    }

    private function readEmbed() : void {
        $embed = (object) [
            'key' => $this->match[self::EMBED_KEY_INDEX][0],
            'type' => 'embed'
        ];
        
        $this->index = $this->index + strlen($this->match[0][0]) + 1;
        
        $operator = $this->match[self::EMBED_OPERATOR_INDEX][0];
        $key_escaped = preg_quote($embed->key);
        $terminator_regex = "/[^\\S\\n]*(${operator})(?!-)[^\\S\\n]*(${key_escaped})[^\\S\\n]*(?=\\n|$)/A";
        
        $begin_line = $this->line;

        while (true) {
            $matched = preg_match(
                $terminator_regex,
                $this->input,
                $terminator_match,
                PREG_OFFSET_CAPTURE | PREG_UNMATCHED_AS_NULL,
                $this->index
            );

            
            if ($matched === 1) {
                $instruction_length = strlen($this->match[0][0]);
                $this->index += $instruction_length + 1;
                $this->line++;
                break;
            }
            
            $end_of_line_column = strpos($this->input, "\n", $this->index);
            
            if ($end_of_line_column === false) {
                throw new Exception("The embed starting in line {$begin_line} is never terminated");
            }
            
            $value = substr($this->input, $this->index, $end_of_line_column - $this->index);
            
            if (isset($embed->value)) {
                $embed->value .= "\n".$value;
            } else {
                $embed->value = $value;
            }

            $this->index = $end_of_line_column + 1;
            $this->line++;
        }

        $this->last_field = null;
        $this->last_section->elements[] = $embed;
    }
    
    private function readField() : void {
        $field = (object) [ 'type' => 'field' ];
        
        if (isset($this->match[self::KEY_UNESCAPED_INDEX][0])) {
            $field->key = $this->match[self::KEY_UNESCAPED_INDEX][0];
        } else {
            $field->key = $this->match[self::KEY_ESCAPED_INDEX][0];
        }
        
        if (isset($this->match[self::FIELD_VALUE_INDEX][0])) {
            $field->value = $this->match[self::FIELD_VALUE_INDEX][0];
        }
        
        $this->last_field = $field;
        $this->last_section->elements[] = $field;
    }
    
    private function readFlag() : void {
        $flag = (object) [ 'type' => 'flag' ];
        
        if (isset($this->match[self::KEY_UNESCAPED_INDEX][0])) {
            $flag->key = $this->match[self::KEY_UNESCAPED_INDEX][0];
        } else {
            $flag->key = $this->match[self::KEY_ESCAPED_INDEX][0];
        }
        
        $this->last_field = null;
        $this->last_section->elements[] = $flag;
    }
    
    private function readItem() : void {
        if ($this->last_field === null) {
            throw new Exception("Item outside field in line {$this->line}");
        }
        
        $item = (object) [ 'type' => 'item '];
        
        if (isset($this->match[self::ITEM_VALUE_INDEX][0])) {
            $item->value = $this->match[self::ITEM_VALUE_INDEX][0];
        }
        
        if (property_exists($this->last_field, 'items')) {
            $this->last_field->items[] = $item;
        } else if (property_exists($this->last_field, 'attributes') || property_exists($this->last_field, 'value')) {
            throw new Exception("Mixed field content in line {$this->line}");
        } else {
            $this->last_field->items = [$item];
        }
    }
    
    private function readSection() : void {
        $section_depth = strlen($this->match[self::SECTION_OPERATOR_INDEX][0]);
        
        if ($section_depth - (count($this->active_section_instructions) - 1) > 1) {
            throw new Exception("Section level skip in line {$this->line}");
        }
        
        $section = (object) [
            'key' => $this->match[self::SECTION_KEY_INDEX][0],
            'type' => 'section'
        ];
        
        if ($section_depth > count($this->active_section_instructions) - 1) {
            $this->last_section->elements[] = $section;
        } else {
            while (count($this->active_section_instructions) - 1 >= $section_depth) {
                array_pop($this->active_section_instructions);
            }
            
            $this->active_section_instructions[count($this->active_section_instructions) - 1]->elements[] = $section;
        }

        $this->last_field = null;
        $this->last_section = $section;
        $this->active_section_instructions[] = $section;
    }
}

function parse(string $input) {
    return (new Parser($input))->parse();
}